#include <String.h>
#include <Vector.h>

typedef Vector<String> Weather_data;

float _wind_direction_min; /* Wind direction min */
float _wind_direction_max; /* Wind direction max */
float _wind_direction_ave; /* Wind direction average */
float _wind_speed_min; /* Wind speed min */
float _wind_speed_max; /* Wind speed max */
float _wind_speed_ave; /* Wind speed average */
float _air_temp; /* Air temperature */
float _relat_humidity; /* Relative humidity */
float _dew_point; /* Dew point */
float _air_pressure; /* Air pressure */

float _rain_accum; /* Rain accumulation */
float _rain_duration; /* Rain duration */
float _rain_intensity; /* Rain intensity */
float _hail_accum; /* Hail accumulation */
float _hail_duration; /* Hail duration */
float _hail_intensity; /* Hail intensity */
float _rain_peak_intensity; /* Rain peak intensity */
float _hail_peak_intensity; /* Hail peak intensity */
float _heating_temp; /* Heating temperature */
float _heating_voltage; /* Heating voltage */
float _supply_voltage; /* Supply voltage */
float _ref_voltage; /* Reference voltage */

void setup(){
    Serial.begin(9600);
    Serial.println("/////////////////////////////");
}

void loop() {
    String str = "0r1,Dn=144D,Dm=161D,Dx=179D,Sn=1.0M,Sm=1.3M,Sx=1.7MM[S";
    Serial.println(str);
    int res = parse_data_weather(str);
    if(res != 0)
        Serial.println("issue with parsing data function");

    str = "0r2,Ta=10.6C,Ua=43.5P,Pa=867.7HOEj";
    Serial.println(str);
    res = parse_data_weather(str);

    str = "0r3,Rc=0.91M,Rd=2050s,Ri=0.0M,Hc=0.0M,Hd=0s,Hi=0.0M,Rp=5.3M,Hp=0.0MG@}";
    Serial.println(str);
    res = parse_data_weather(str);

    str = "0r5,Th=14.6C,Vh=26.2V,Vs=25.9V,Vr=3.629VJzS";
    Serial.println(str);
    res = parse_data_weather(str);
    while(1);
}

int parse_data_weather(String str)
{
    String storage_array[10];
    Weather_data _weather_data;
    _weather_data.setStorage(storage_array);

    // parse the String with the delimiter
    char *token = strtok(&str[0], ",");
    while(token)
    {
        // add the token into the vector
        _weather_data.push_back(token);
        token = strtok(NULL, ",");
    }
    int n = _weather_data.size();
    // vector size should be at least 4
    if (n < 4)
    {
        Serial.println("vector size is smaller than expected ");
        _weather_data.clear();
        return -1;
    }
    String tmp = _weather_data[n-1];
    // remove the last token to separate the CRC
    _weather_data.pop_back();
    
    // copy of the String from a position to another
    String msg = tmp.substring(0, tmp.length()-3);
    String _crc = tmp.substring(tmp.length()-3, tmp.length());

    // add the String without the CRC into the vector
    _weather_data.push_back(msg);

    for (String i : _weather_data)
        Serial.println(i);

    Serial.print("CRC=");
    Serial.println(_crc);

    // extract parameters
    int res = get_param_weather(_weather_data);
    if (res != 0)
    {
        Serial.println("issue with weather parameters");
        _weather_data.clear();
        return -1;
    }

    _weather_data.clear();
    return 0;
}

int get_param_weather(Weather_data v)
{
    String command = v[0].substring(1, v[0].length());
    String tmp;
    String value;
    if(!command.compareTo("r1"))
    {
        for(int i = 1; i < v.size() ; i++)
        {
            String data = v[i];
            char *tmp = strtok(&data[0], "=");
            value = v[i].substring(3, v[i].length()-1);
            if(!String(tmp).compareTo("Dn"))
            {
                _wind_direction_min = value.toFloat();
                Serial.print(" wind_direction_min = ");
                Serial.println(_wind_direction_min);
            }
            else if(!String(tmp).compareTo("Dm"))
            {
                _wind_direction_ave = value.toFloat();
                Serial.print(" wind_direction_ave = ");
                Serial.println(_wind_direction_ave);
            }
            else if(!String(tmp).compareTo("Dx"))
            {
                _wind_direction_max = value.toFloat();
                Serial.print(" wind_direction_max = ");
                Serial.println(_wind_direction_max);
            }
            else if(!String(tmp).compareTo("Sn"))
            {
                _wind_speed_min = value.toFloat();
                Serial.print(" wind_speed_min = ");
                Serial.println(_wind_speed_min);
            }
            else if(!String(tmp).compareTo("Sm"))
            {
                _wind_speed_ave = value.toFloat();
                Serial.print(" wind_speed_ave = ");
                Serial.println(_wind_speed_ave);
            }
            else if(!String(tmp).compareTo("Sx"))
            {
                _wind_speed_max = value.toFloat();
                Serial.print(" wind_speed_max = ");
                Serial.println(_wind_speed_max);
            }
            else
            {
                Serial.println("unrecognized data");
                return -1;
            }
        }
    }
    else if(!command.compareTo("r2"))
    {
        for(int i = 1; i < v.size() ; i++)
        {          
            String data = v[i];
            char *tmp = strtok(&data[0], "=");
            value = v[i].substring(3, v[i].length()-1);
            if(!String(tmp).compareTo("Ta"))
            {
                _air_temp = value.toFloat();
                Serial.print(" air_temp = ");
                Serial.println(_air_temp);
            }
            else if(!String(tmp).compareTo("Ua"))
            {
                _relat_humidity = value.toFloat();
                Serial.print(" relat_humidity = ");
                Serial.println(_relat_humidity);
                
                _dew_point = dew_point_calculation(_air_temp, _relat_humidity);
                Serial.print(" dew_point = ");
                Serial.println(_dew_point);
            }
            else if(!String(tmp).compareTo("Pa"))
            {
                _air_pressure = value.toFloat();
                Serial.print(" air_pressure = ");
                Serial.println(_air_pressure);
            }
            else
            {
                Serial.println("unrecognized data");
                return -1;
            }
        }
    }
    else if(!command.compareTo("r3"))
    {
        for(int i = 1; i < v.size() ; i++)
        {
            String data = v[i];
            char *tmp = strtok(&data[0], "=");
            value = v[i].substring(3, v[i].length()-1);
            if(!String(tmp).compareTo("Rc"))
            {
                _rain_accum = value.toFloat();
                Serial.print(" rain_accum = ");
                Serial.println(_rain_accum);
            }
            else if(!String(tmp).compareTo("Rd"))
            {
                _rain_duration = value.toFloat();
                Serial.print(" rain_duration = ");
                Serial.println(_rain_duration);
            }
            else if(!String(tmp).compareTo("Ri"))
            {
                _rain_intensity = value.toFloat();
                Serial.print(" rain_intensity = ");
                Serial.println(_rain_intensity);
            }
            else if(!String(tmp).compareTo("Hc"))
            {
                _hail_accum = value.toFloat();
                Serial.print(" hail_accum = ");
                Serial.println(_hail_accum);
            }
            else if(!String(tmp).compareTo("Hd"))
            {
                _hail_duration = value.toFloat();
                Serial.print(" hail_duration = ");
                Serial.println(_hail_duration);
            }
            else if(!String(tmp).compareTo("Hi"))
            {
                _hail_intensity = value.toFloat();
                Serial.print(" hail_intensity = ");
                Serial.println(_hail_intensity);
            }
            else if(!String(tmp).compareTo("Rp"))
            {
                _rain_peak_intensity = value.toFloat();
                Serial.print(" rain_peak_intensity = ");
                Serial.println(_rain_peak_intensity);
            }
            else if(!String(tmp).compareTo("Hp"))
            {
                _hail_peak_intensity = value.toFloat();
                Serial.print(" hail_peak_intensity = ");
                Serial.println(_hail_peak_intensity);
            }
            else
            {
                Serial.println("unrecognized data");
                return -1;
            }
        }
    }
    else if(!command.compareTo("r5"))
    {
        for(int i = 1; i < v.size() ; i++)
        {
            String data = v[i];
            char *tmp = strtok(&data[0], "=");
            value = v[i].substring(3, v[i].length()-1);
            if(!String(tmp).compareTo("Th"))
            {
                _heating_temp = value.toFloat();
                Serial.print(" heating_temp = ");
                Serial.println(_heating_temp);
            }
            else if(!String(tmp).compareTo("Vh"))
            {
                _heating_voltage = value.toFloat();
                Serial.print(" heating_voltage = ");
                Serial.println(_heating_voltage);
            }
            else if(!String(tmp).compareTo("Vs"))
            {
                _supply_voltage = value.toFloat();
                Serial.print(" supply_voltage = ");
                Serial.println(_supply_voltage);
            }
            else if(!String(tmp).compareTo("Vr"))
            {
                _ref_voltage = value.toFloat();
                Serial.print(" ref_voltage = ");
                Serial.println(_ref_voltage);
            }
            else
            {
                Serial.println("unrecognized data");
                return -1;
            }
        }
    }
    else {
        Serial.println("unrecognized data");
        return -1;
    }
    return 0;
}

float dew_point_calculation(float temp, float hum)
{
    float alpha = 6.112;
    float beta = 17.62;
    float lambda = 243.12;
    return ((lambda*(log(hum/100)+((beta*temp)/(lambda+temp))))/
            (beta-(log(hum/100)+((beta*temp)/(lambda+temp)))));
}
